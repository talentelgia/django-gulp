require('./gulp/tasks/styles');
require('./gulp/tasks/watch');


// File Paths
var CSS_DIST_PATH = './app/temp/styles/';
var SCRIPTS_PATH = 'public/scripts/**/*.js';
var SCSS_PATH = './app/assets/styles/styles.scss';
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');

var babel = require('gulp-babel');



// styles CSS
gulp.task('styles', function () {
    console.log('Starting styles task!!');
    return gulp.src(['public/css/reset.css', CSS_PATH])
        .on('error', function (errorInfo) {
            console.log(errorInfo.toString());
            this.emit('end');
        })
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(concat('styles.css'))
        .pipe(minifyCss())
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest(DIST_PATH))
        .pipe(livereload());
});

//styles For SCSS
// gulp.task('styles', function () {
//     console.log('Starting styles task!!');
//     return gulp.src('designate_project/static/scss/styles.scss')
//         .pipe(plumber(function (err) {
//             console.log('Styles Task Error');
//             console.log(err);
//             this.emit('end)');
//         }))
//         .pipe(sourcemaps.init())
//         .pipe(autoprefixer())
//         .pipe(sass({
//             outputStyle: 'compressed'
//         }))
//         .pipe(sourcemaps.write('../maps'))
//         .pipe(gulp.dest(DIST_PATH))
//         .pipe(livereload());
// });

// gulp.task('styles', function () {
//     console.log('Starting styles task!!');
//     return gulp.src('designate_project/static/scss/styles.scss')
//         .pipe(plumber(function (err) {
//             console.log('Styles Task Error');
//             console.log(err);
//             this.emit('end)');
//         }))
//         .pipe(sourcemaps.init())
//         .pipe(autoprefixer())
//         .pipe(sass({
//             outputStyle: 'compressed'
//         }))
//         .pipe(sourcemaps.write('../maps'))
//         .pipe(gulp.dest(CSS_DIST_PATH))
//         .pipe(livereload());
// });

// gulp.task('default', ['styles']);

// var gulp        = require('gulp');
// var browserSync = require('browser-sync');
// var reload      = browserSync.reload;

// gulp.task('default', function() {
//     browserSync.init({
//         notify: false,
//         proxy: "localhost:8000"
//     });
//     gulp.watch(['./**/*.{scss,css,html,py,js}'], reload);
// });


var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var exec = require('child_process').exec;

gulp.task('sass', function() {
  return gulp.src('static/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('static/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});


gulp.task('styles', function () {
    console.log('Starting styles task!!');
    return gulp.src(['static/css/reset.css', CSS_PATH])
        .on('error', function (errorInfo) {
            console.log(errorInfo.toString());
            this.emit('end');
        })
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(concat('styles.css'))
        .pipe(minifyCss())
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest(DIST_PATH))
        .pipe(livereload());
});

gulp.task('runserver', function() {
  var proc = exec('python manage.py runserver')
})

gulp.task('browserSync', ['runserver'], function() {
  browserSync.init({
    notify: false,
    port: 8000,
    proxy: 'localhost:8000'
  })
});

gulp.task('watch', ['browserSync', 'sass'], function() {
  gulp.watch('static/scss/*.scss', ['sass']);
  gulp.watch('static/js/**/*.js', browserSync.reload);
  gulp.watch('templates/**/*.html', browserSync.reload);
})

gulp.task('default', ['runserver','browserSync','watch']);

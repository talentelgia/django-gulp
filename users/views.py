from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from django.template import loader
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from .forms import UserForm

"""
Method:             index
Developer:          Aziz
Created Date:       25-06-2018
Purpose:            Index
Params:             []
Return:             user hash []
"""
def index(request):
    context = {}
    form = UserForm()
    return render(request, 'index.html', {'form': form})
"""end function index"""

def about(request):
    context = {}
    return render(request, 'index.html', context)

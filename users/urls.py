from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views
# from users import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^about/', views.about, name='about'),
]

require('./gulp/tasks/styles');
require('./gulp/tasks/watch');

var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');
var browserSync = require('browser-sync').create();
var exec = require('child_process').exec;

gulp.task('sass', function() {
  return gulp.src('static/scss')
    .pipe(sass())
    .pipe(gulp.dest('static/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('sass-js', function() {
  return gulp.src('static/js')
    .pipe(sass())
    .pipe(gulp.dest('static/js'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('runserver', function() {
  var proc = exec('python manage.py runserver')
})

gulp.task('browserSync', ['runserver'], function() {
  browserSync.init({
    notify: false,
    port: 8000,
    proxy: 'localhost:8000'
  })
});

gulp.task('watch', ['browserSync', 'sass', 'sass-js'], function() {
  gulp.watch('static/scss/**/*.scss', ['sass']);
  gulp.watch('static/js/*.js', ['sass-js']);
  gulp.watch('static/css/**/*.css', browserSync.reload);
  gulp.watch('templates/**/*.html', browserSync.reload);
})

gulp.task('default', ['runserver','browserSync','watch']);

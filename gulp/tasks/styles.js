/* Gulp plugins required in styles task */

var gulp = require("gulp");
var sass = require('gulp-sass');
var gcmq = require('gulp-group-css-media-queries');


/* Variables for destinations */

var CSS_DIST_PATH = './app/temp/styles/';
var SCSS_PATH = './app/assets/styles/styles.scss';


/* styles task, convert scss to compressed styles.css file in temp folder */
gulp.task("styles", function() {
  return gulp.src(SCSS_PATH)
    .pipe(sass())
    .on('error', function (errorInfo) {
      console.log(errorInfo.toString());
      this.emit('end');
    })
    .pipe(gcmq())
    .pipe(gulp.dest(CSS_DIST_PATH));
});


// .pipe(sass({
//     outputStyle: 'compressed'
// }))

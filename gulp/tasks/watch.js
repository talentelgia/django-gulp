/* Gulp plugins required in watch task */

var gulp = require("gulp");
var watch = require("gulp-watch");
var browserSync = require("browser-sync").create();


/* Gulp Watch Task, watches HTML and SCSS for changes, whenever there is a changes
a new styles.css file is made in the temp folder, and gulp injects the css changes
into the browser with browserSync */

gulp.task("watch", function() {

  // browserSync, nofications is turned of
  browserSync.init({
    notify: false,
    server: {
      baseDir: "app"
    }
  });

  // Browser reloads when a HTML change is saved.
  watch("./app/**/*.html", function() {
    browserSync.reload();
  });

  // When a SCSS change is saved, Gulp watch starts the cssInject Task
  watch("./app/assets/**/*.scss", function() {
    gulp.start("cssInject");
  });

});

/* This task injects css into the browser, without reloading,
depending on styles task before executing */
gulp.task("cssInject", ['styles'], function () {
  return gulp.src("./app/temp/styles/styles.css")
    .pipe(browserSync.stream());

});
